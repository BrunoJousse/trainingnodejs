//Routes for /evenements
import express from 'express';
import bodyParser from "body-parser";

import * as service from './services.js';

const postRoutes = express.Router();
postRoutes.use(bodyParser.json());

//Get all posts
postRoutes.get("/", (req, res) => {
    let condition = {};
    if (req.query.id) {
        //TODO condition
    }
    service.getPosts(condition).then(
        posts => {
            res.status(200).json(posts);
        },
        err => {
            console.error("Error on: \"Get all posts\":" + err);
            res.status(500).send("Error while fetching posts.");
            return;
        }
    );
});

//Get post
postRoutes.get("/:id", (req, res) => {
    service.getPost(req.params.id).then(
        post => {
            res.status(200).json(post);
        },
        err => {
            console.error("Error on: \"Get post\":" + err);
            res.status(500).send("Error while fetching post.");
            return;
        }
    );
})

//Post post
postRoutes.post("/", (req, res) => {
    if (req.body && req.body.title && req.body.description && req.body.id_user && !req.body.id) {
        service.createPost(req.body).then(
            insertId => {
                res.status(200).json({
                    success: true,
                    id: insertId
                })
            },
            err => {
                console.error("Error on: \"Post post\":" + err);
                res.status(500).send("Server error while creating post.");
                return;
            }
        );
    }
    else {
        res.status(400).send("Bad parameters for creating a post.");
    }
});

//Update post
postRoutes.put("/:id", (req, res) => {
    if (req.body && req.body.title && req.body.description) {
        service.updatePost(req.params.id, req.body).then(
            id => {
                res.status(200).json(id);
            },
            err => {
                console.error("Error on: \"Update post\":" + err);
                res.status(500).send("Error while updating post.");
                return;
            }
        );
    }
    else {
        res.status(400).send("Bad parameters for updating a post.");
    }
})

//Delete post
postRoutes.delete("/:id", (req, res) => {
    service.deletePost(req.params.id).then(
        id => {
            res.status(200).json(id);
        },
        err => {
            console.error("Error on: \"Delete post\":" + err);
            res.status(500).send("Error while deleting post.");
            return;
        }
    );
})


export default postRoutes;