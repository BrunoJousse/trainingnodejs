//Routes for /evenements
import express from 'express';
import bodyParser from "body-parser";

import * as service from './services.js';

const userRoutes = express.Router();
userRoutes.use(bodyParser.json());

//Get all users
userRoutes.get("/", (req, res) => {
    let condition = {};
    if (req.query.research) {
        //TODO condition
    } 
    service.getUsers(condition).then(
        users => {
            res.status(200).json(users);
        },
        err => {
            console.error("Error on: \"Get all users\":" + err);
            res.status(500).send("Error while fetching users.");
            return;
        }
    );
});

//Get post
userRoutes.get("/:id", (req, res) => {
    service.getUser(req.params.id).then(
        user => {
            res.status(200).json(user);
        },
        err => {
            console.error("Error on: \"Get user\":" + err);
            res.status(500).send("Error while fetching user.");
            return;
        }
    );
});

//Post user
userRoutes.post("/", (req, res) => {
    if (req.body && req.body.firstName && req.body.lastName && !req.body.id) {
        service.createUser(req.body).then(
            insertId => {
                res.status(200).json({
                    success: true,
                    id: insertId
                })
            },
            err => {
                console.error("Error on: \"Post user\":" + err);
                res.status(500).send("Server error while creating user.");
                return;
            }
        );
    }
    else {
        res.status(400).send("Bad parameters for creating user.");
    }
});

export default userRoutes;