import Sequelize from "sequelize"
import db from "../db.js";

const User = db.define("user", {
    firstName: {
        type: Sequelize.STRING,
        allowNull: false
    },
    lastName: {
        type: Sequelize.STRING,
        allowNull: false
    }
})

export default User;

/*
 * Parent.hasMany(Child, { foreignKey: 'parent_id' }); // note the 'foreign_key' parameter
 * Child.belongsTo(Parent, { foreignKey: 'parent_id' }); // and here as well
 */