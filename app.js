import express from 'express';
import bodyParser from "body-parser";
import dotenv from "dotenv";
import cors from "cors";

import db from './db.js';
import userRoutes from './users/routes.js';
import postRoutes from "./posts/routes.js";

//Get env variables
dotenv.config();

// Server express.js
const app = express();

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cors());

app.use('/posts', postRoutes);
app.use('/users', userRoutes);

db.authenticate()
.then(() => {
    console.log("Connection has been established successfully.");

    let server = app.listen(process.env.PORT, () => {
        console.log(`server running on port ${process.env.PORT}`);
    });
})
.catch(err => {
    console.error("Unable to connect to the databse: ", err);
});