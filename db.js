import dotenv from "dotenv";
import Sequelize from 'sequelize';

dotenv.config();

const db = new Sequelize(process.env.DB_DATABASE, process.env.DB_USER, process.env.DB_PASSWORD, {
    host: process.env.DB_HOST,
    dialect: 'mysql',
    pool: {
        max: 30,
    },
    define: {
        //Sequelize convert automatically numeral into plural. This cancels it.
        freezeTableName: true,
        //Transform ColumnName into column_name
        underscored: true,
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
});

export default db;