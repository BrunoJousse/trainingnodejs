This project is only a training project to learn NodeJS

File .env contains:
PORT (web server port)
DB_HOST (address to database)
DB_USER (admin username)
DB_PASSWORD (admin password)
DB_DATABASE (database name)
DB_PORT (database port)