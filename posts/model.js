import Sequelize from "sequelize"
import db from "../db.js";
import User from "../users/model.js";

const Post = db.define("post", {
    title: {
        type: Sequelize.STRING,
        allowNull: false
    },
    description: {
        type: Sequelize.STRING,
        allowNull: false
    }
})

Post.belongsTo(User, { foreignKey: "id_user" });
User.hasMany(Post, { foreignKey: 'user_id' });

export default Post;