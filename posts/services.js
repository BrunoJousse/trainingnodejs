// Services for posts
import Post from './model.js';

//Return posts
export async function getPosts(condition) {
    try {
        const rows = await Post.findAll(condition);
        console.log("Data received from DB: " + JSON.stringify(rows));

        return rows;
    }
    catch (err) {
        throw err;
    }
}

//Return post
export async function getPost(id) {
    try {
        const post = await Post.findOne({
            where: {
                id: id
            }
        });
        console.log("Data received from DB: " + JSON.stringify(post));

        return post;
    }
    catch (err) {
        throw err;
    }
}

//Create post
export async function createPost(post) {
    try {
        const res = await Post.create(post);

        console.log("Created post:", JSON.stringify(res));
        return res.id;
    }
    catch (err) {
        throw err;
    }
}

//Update post
export async function updatePost(id, post) {
    try {
        const returnedId = await Post.update({
            title: post.title,
            description: post.description
        }, {
            where: {
                id: id
            }
        });

        console.log("Updated post id:", JSON.stringify(returnedId));
        return returnedId;
    }
    catch (err) {
        throw err;
    }
}

//Delete post
export async function deletePost(id) {
    try {
        const res = await Post.destroy({
            where: {
                id: id
            }
        });

        console.log("Deleted post:", JSON.stringify(res));
        return res;
    }
    catch (err) {
        throw err;
    }
}
