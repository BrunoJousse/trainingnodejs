// Services for users
import User from './model.js';

//Return users
export async function getUsers(condition) {
    try {
        const rows = await User.findAll(condition);
        console.log("Data received from DB: " + JSON.stringify(rows));

        return rows;
    }
    catch(err){
        throw err;
    }
}

//Return user
export async function getUser(id) {
    try {
        const rows = await User.findOne({
            where: {
                id: id
            }
        });
        console.log("Data received from DB: " + JSON.stringify(rows));

        return rows;
    }
    catch (err) {
        throw err;
    }
}

//Create user
export async function createUser(user) {
    try {
        const res = await User.create(user);

        console.log("Last insert ID:", res.id);
        return res.id;
    }
    catch (err) {
        throw err;
    }
}
